﻿using MinesweeperClasses;
using System;

namespace Milestone2Minesweeper
{
    class Program
    {

        static MinesweeperGameDriver game = new MinesweeperGameDriver();

        static void Main(string[] args)
        {
            game.playGame();
        }
    }
}
