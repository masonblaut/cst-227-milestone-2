﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinesweeperClasses
{
    public class MinesweeperGameDriver : IPlayable
    {
        static public int boardSize = 4;
        static public Board myBoard = new Board(boardSize);
        static public bool gameOver = false;

        static int gameGoal = boardSize*boardSize;
        static int gameProgress = 0;

        public void playGame()
        {
            myBoard.assignBombs();
            setGameGoal();


            while (gameOver != true)
            {
                Console.Clear();
                revealBoard();
                Console.WriteLine("Game Goal: "+gameGoal+" Game Progress: "+gameProgress);
                gameWin();
                visitSquare();
            }

        }

        public void revealBoard()
        {
            for (int r = 0; r < myBoard.Size; r++)
            {
                for (int c = 0; c < myBoard.Size; c++)
                {
                    if (myBoard.theGrid[r, c].hasBomb && myBoard.theGrid[r,c].isVisited)
                    {
                        Console.Write(" x ");
                    }
                    else if (myBoard.theGrid[r, c].nextToBomb && myBoard.theGrid[r,c].isVisited)
                    {
                        Console.Write(" " + myBoard.theGrid[r, c].nextCount + " ");
                    }
                    else if (myBoard.theGrid[r, c].isVisited)
                    {
                        Console.Write(" ~ ");
                    }
                    else
                    {
                        Console.Write(" ? ");
                    }
                }
                Console.WriteLine();
            }
        }

        public void visitSquare()
        {
            int row = 0;
            int col = 0;
            Console.Write("Visit a row: ");
            row = int.Parse(Console.ReadLine());
            Console.Write("Visit a column: ");
            col = int.Parse(Console.ReadLine());
            myBoard.theGrid[row, col].visited();

            if(myBoard.theGrid[row, col].isVisited && myBoard.theGrid[row, col].hasBomb)
            {
                gameLose();
            }
            else
            {
                gameProgress++;
            }
        }

        public void gameLose()
        {
            Console.WriteLine("BOOM!!! Game Over");
            revealAllBoard();
            gameOver = true;
        }

        public void revealAllBoard()
        {
            for (int r = 0; r < myBoard.Size; r++)
            {
                for (int c = 0; c < myBoard.Size; c++)
                {
                    if (myBoard.theGrid[r, c].hasBomb)
                    {
                        Console.Write(" x ");
                    }
                    else if (myBoard.theGrid[r, c].nextToBomb)
                    {
                        Console.Write(" " + myBoard.theGrid[r, c].nextCount + " ");
                    }
                    else
                    {
                        Console.Write(" ~ ");
                    }
                }
                Console.WriteLine();
            }
        }

        public void setGameGoal()
        {
            for (int r = 0; r < myBoard.Size; r++)
            {
                for (int c = 0; c < myBoard.Size; c++)
                {
                    if (myBoard.theGrid[r, c].hasBomb)
                    {
                        gameGoal--;
                    }
                }
            }
        }

        public void gameWin()
        {
            if (gameGoal == gameProgress)
            {
                Console.WriteLine("Game Over!!! You Win!");
            }
        }
    }
}
