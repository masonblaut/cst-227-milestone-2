﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinesweeperClasses
{
    public class Cell
    {


        public int RowNumber { get; set; }
        public int ColNumber { get; set; }

        public Boolean hasBomb = false;
        public Boolean nextToBomb = false;
        public int nextCount = 0;

        public Boolean isVisited = false;

        public Cell(int r, int c)
        {
            RowNumber = r;
            ColNumber = c;
        }

        public void setNextToBomb()
        {
            nextToBomb = true;
            nextCount++;
        }

        public void visited()
        {
            isVisited = true;
        }


    }
}
